package com.webcontroller.briefdeux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@SpringBootApplication
public class BriefDeuxApplication {

    public static void main(String[] args) {

        SpringApplication.run(BriefDeuxApplication.class, args);
    }

    @Controller
    public class HelloWorld {

        @GetMapping("/hello-world")
        @ResponseBody
        public String getHello() {
            return "Hello World!!";
        }
    }

    @Controller
    public class HelloTo {
        @GetMapping("/hello-to")
        @ResponseBody
        public String getHelloTo(@RequestParam(defaultValue = "Simplon") String name
                                 ) {
            return "Hello " + name +  "!!";
        }
    }

}

